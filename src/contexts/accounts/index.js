import User from './User';

async function getUser() {
    const user = await User.find({});
    // throw new CustomError('Unauthorized', {}, 401);
    return user;
}

export default {
    getUser,
}