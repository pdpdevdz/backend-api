import Accounts from '../../contexts/accounts';
import fallback from '../../lib/fallback';

async function show(req, res) {
    try {
        const users = await Accounts.getUser();
        res.send({users})
    } catch (e) {
        LOG('UserController:show', e);
        fallback(res, e);
    }
}

export default {
    show,
}