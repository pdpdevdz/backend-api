import userRoutes from './user';

export default (app) => {
    app.use('/users', userRoutes);
}