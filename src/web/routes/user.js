import { Router } from 'express';
import Controller from '../controllers/user';

const router = Router();

router.get('/:username', Controller.show);

export default router;