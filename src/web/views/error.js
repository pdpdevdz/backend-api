
function renderError(res, reason, errors = {}, status = 400) {
    res.status(status).send({reason, errors});
  }
  
  
  export default {
    renderError,
  }
  