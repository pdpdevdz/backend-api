import './load';
import express from 'express';
import cors from 'cors';
import connectToDB from './src/db/connect';
import routes from './src/web/routes';

connectToDB();

const app = express();

if(__DEV__) {
    app.use(cors());
}

app.get('/', async (req, res) => {
    res.send({hello: true})
});

routes(app);

const PORT = process.env.PORT || 4000;
app.listen(PORT, (err) => {
    if(err) {
        console.log("starting server failed: ", err);
    }

    console.log('server running at port ', PORT);
});

